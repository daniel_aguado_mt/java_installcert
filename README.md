# Example of use

``` bash
url=url_with_https"
binUrl="https://bitbucket.org/daniel_aguado_mt/java_installcert/downloads/java_installcert-1.0.jar"
file="/opt/InstallCert.jar"

if [ -f "$file" ]
then
	echo "Certificates installed. Skipped"
else
	echo "Installing certificate from $url"
    curl -sLo $file $binUrl
    java -jar $file $url:443
fi
```